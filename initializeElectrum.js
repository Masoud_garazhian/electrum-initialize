var sys = require('sys')
var exec = require('child_process').exec;
const readline = require('readline');
function puts(error, stdout, stderr) { sys.puts(stdout) }

const sleep = async (milliseconds) => {
    await new Promise(resolve => setTimeout(resolve, milliseconds));
}
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const stop = async () => {
    console.log(`>>>>> ${new Date().toUTCString()}  Stopping Electrum`);
    exec("electrum stop", puts);
    // console.log("Electrum Stopped");

}
const start = async () => {
    console.log(`>>>>> ${new Date().toUTCString()}  Starting Electrum`);
    exec("electrum daemon -d", puts);
    console.log(`>>>>> ${new Date().toUTCString()}  Electrum Started`);

}

const load = async () => {
    console.log(`>>>>> ${new Date().toUTCString()}  Loading Wallet`);
    exec("electrum load_wallet", puts);
    console.log(`>>>>> ${new Date().toUTCString()}  Wallet Loaded`);
}

stop();                                         //#1
setTimeout(function () { start(); }, 3000);     //#2
setTimeout(function () { load(); }, 4000);      //#3
console.log(`>>>>> ${new Date().toUTCString()} Done!` );

// setTimeout(function () { exec("electrum listaddresses", puts); }, 10000);

rl.question(`>>>>> ${new Date().toUTCString()} This will task running by pm2 !!!` , (answer) => {
    rl.close();
});
